export interface IClockifyUsers {
  id: number;
  name: string;
  img: string;
  quantity: number;
  rate: number;
}
