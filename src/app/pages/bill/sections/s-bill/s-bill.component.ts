import { Component, EventEmitter, Input, Output } from '@angular/core';
import { IEmployee } from '@shared/models/employee.model';

@Component({
  selector: 's-bill',
  templateUrl: './s-bill.component.html',
  styleUrls: ['./s-bill.component.scss'],
})
export class BillComponent {
  @Input() public employees: Array<IEmployee>;
  @Input() public bill: Array<IEmployee>;
  @Input() public searchText: string;

  @Output() public pushToBill: EventEmitter<void> = new EventEmitter();
  @Output() public sendBill: EventEmitter<void> = new EventEmitter();
  @Output() public clockifyApiResponse: EventEmitter<void> = new EventEmitter();
  @Output() public invoicelyApiResponse: EventEmitter<void> = new EventEmitter();

  public addToBill(item: IEmployee): void {
    const index = this.employees.findIndex(
      x => x.id == item.id,
    );
    this.bill.push(this.employees[index]);
    this.employees.splice(index, 1);
  }

  public removeFromBill(item: IEmployee): void {
    const index = this.bill.findIndex(
      x => x.id == item.id,
    );
    this.employees.push(this.bill[index]);
    this.bill.splice(index, 1);
  }

  public filterCondition(item: IEmployee) {
    return item.name.toLowerCase().indexOf(this.searchText.toLowerCase()) != -1;
  }
}
