export class BillData {
  public billTitle: string;
  public billDescription: string;
  public billDate: string;
  public billItem: string;
  public billQuantity: number;
  public billRate: number;
}
