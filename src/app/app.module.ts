import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { Pages } from '@pages/index';
import { Services } from '@services/index';
import { BackendInterceptorService } from '@services/interceptors/backend-interceptor.service';
import { AppMaterialModule } from './app-material.module';

import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/components/header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    Pages,
    HeaderComponent,
  ],
  imports: [
    AppMaterialModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [
    Services, {
      provide: HTTP_INTERCEPTORS,
      useClass: BackendInterceptorService,
      multi: true,
    }],
  bootstrap: [AppComponent],
})
export class AppModule {
}
