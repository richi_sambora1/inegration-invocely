import { BillComponent } from '@pages/bill/sections/s-bill/s-bill.component';
import { ViewBillComponent } from '@pages/bill/view/view-bill.component';

export const BillComponents = [
  BillComponent,
  ViewBillComponent,
];
