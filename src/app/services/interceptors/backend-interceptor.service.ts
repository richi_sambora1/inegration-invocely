import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

const usersData = [
    {
      'id': 1,
      'img': 'assets/img/1.png',
      'name': 'Никита Суфранович',
      'rate': 10,
      'quantity': 30,
    },
    {
      'id': 2,
      'img': 'assets/img/2.png',
      'name': 'Артур Курин',
      'rate': 3,
      'quantity': 12,
    },
    {
      'id': 3,
      'img': 'assets/img/3.png',
      'name': 'Александр Грин',
      'rate': 3,
      'quantity': 23,
    },
    {
      'id': 4,
      'img': 'assets/img/1.png',
      'name': 'Константин Яковлев',
      'rate': 4,
      'quantity': 12,
    },
    {
      'id': 5,
      'img': 'assets/img/2.png',
      'name': 'Дмитрий Блыщик',
      'rate': 12,
      'quantity': 32,
    },
    {
      'id': 6,
      'img': 'assets/img/3.png',
      'name': 'Александр Делен',
      'rate': 12,
      'quantity': 31,
    },
    {
      'id': 7,
      'img': 'assets/img/1.png',
      'name': 'Каштан Алешков',
      'rate': 12,
      'quantity': 31,
    },
    {
      'id': 8,
      'img': 'assets/img/2.png',
      'name': 'Айзек Азимов',
      'rate': 12,
      'quantity': 31,
    },
    {
      'id': 9,
      'img': 'assets/img/2.png',
      'name': 'Китай Японов',
      'rate': 2,
      'quantity': 12,
    },
    {
      'id': 10,
      'img': 'assets/img/1.png',
      'name': 'Шербак Алёхов',
      'rate': 12,
      'quantity': 28,
    },
    {
      'id': 11,
      'img': 'assets/img/3.png',
      'name': 'Титан Магнитов',
      'rate': 13,
      'quantity': 32,
    },
    {
      'id': 12,
      'img': 'assets/img/1.png',
      'name': 'Мерцан Стеклов',
      'rate': 18,
      'quantity': 42,
    },
    {
      'id': 13,
      'img': 'assets/img/1.png',
      'name': 'Автоматов Максим',
      'rate': 7,
      'quantity': 11,
    },
];

@Injectable({
  providedIn: 'root',
})
export class BackendInterceptorService implements HttpInterceptor {
  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.method === 'GET' && req.url === 'http://localhost:4200/users') {
      return of(new HttpResponse({status: 200, body: usersData}));
    }
    next.handle(req);
  }
}
