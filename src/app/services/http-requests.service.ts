import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BillData } from '@shared/models/bill-data.model';
import { IClockifyUsers } from '@shared/models/clockifyUsers.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class HttpRequestsService {
  public billData: BillData = new BillData();
  public customHeaders = new HttpHeaders().set('Access-Control-Allow-Origin', '*');

  public url = 'https://api.clockify.me/api/v1/user';
  public accessToken = 'XnpqvCa2YzyVSHbl';
  public workspaceId = '5e2feaa6b128ac31f2589da3';

  public invoicelyUrl = 'https://invoicely.com/perform_login';
  public invoicelyData = {
    email_address: 'nikita.sufranovich@gmail.com',
    password: 'detroit313',
  };

  constructor(
    public readonly http: HttpClient,
  ) { }

  public testClockifyApi$(): Observable<IClockifyUsers> {
    return this.http.get<IClockifyUsers>(this.url)
      .pipe(data => {
        return data;
      });
  }

  public testInvoicelyApi$(): Observable<any> {
    const body = new FormData();
    body.append('email_address', 'nikita.sufranovich@gmail.com');
    body.append('password', 'detroit313');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'multipart/form-data',
        'Access-Control-Allow-Origin': '*',
      }),
    };

    return this.http.post(this.invoicelyUrl, body, httpOptions)
      .pipe(data => {
        return data;
      });
  }

  public getUsers$(usersUrl): Observable<any> {
    return this.http.get<any>(usersUrl)
      .pipe(data => {
        return data;
      });
  }

  public postLoginInInvoicely$(loginUrl, loginData): Observable<any> {
    let customHeaders = new HttpHeaders();
    customHeaders = customHeaders.set('Access-Control-Allow-Origin', '*');

    return this.http.post<any>(loginUrl, loginData, {headers: customHeaders})
      .pipe(data => {
        return data;
      });
  }

  public postData$(postDataUrl, postData): Observable<BillData> {
    return this.http.post<BillData>(postDataUrl, postData)
      .pipe(data => {
        return data;
      });
  }
}
