export interface IEmployee {
  id: number;
  img: string;
  name: string;
  rate: number;
  quantity: number;
}
