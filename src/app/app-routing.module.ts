import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ViewBillComponent } from '@pages/bill/view/view-bill.component';


const routes: Routes = [
  { path: '', redirectTo: '/bill', pathMatch: 'full' },
  { path: 'bill', component: ViewBillComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
