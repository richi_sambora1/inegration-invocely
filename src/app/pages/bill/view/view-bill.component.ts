import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { HttpRequestsService } from '@services/http-requests.service';

@Component({
  selector: 'v-bill',
  templateUrl: './view-bill.component.html',
})
export class ViewBillComponent implements OnInit {
  public employees: Object;
  public bill = [];
  public searchText = '';

  constructor(
     public readonly httpRequests: HttpRequestsService,
     public readonly http: HttpClient,
  ) { }

  public ngOnInit(): void {
    this.httpRequests.getUsers$('http://localhost:4200/users')
      .subscribe(response => {
        this.employees = response;
      });
  }

  public sendBill(): void {
    console.log(this.bill);
  }

  public pushToBill(): void {
    const abs = this.employees;
    this.bill.push(abs[0]);
  }

  public clockifyApiResponse(): void {
    this.httpRequests.testClockifyApi$()
      .subscribe(response => {
        console.log(response);
      });
  }

  public invoicelyApiResponse(): void {
    this.httpRequests.testInvoicelyApi$()
      .subscribe(response => {
        console.log(response);
      });
  }
}
